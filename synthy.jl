module Synthy

# using Statistics

# export Note, transform

struct Note
    stamp::Int64
    key::UInt8
    velocity::UInt8
    keep_alive::Bool
    end_velocity::UInt8
    end_stamp::Int64
end

function transform(samples::Array{Float32,2}, notes::Vector{Note}, sample_rate::Number, start_time::Int64)
    samples .= 0

    for note_i in axes(notes, 1)
        note = notes[note_i]

        keep_alive = true
        note_time = note.stamp - 900_000_000

        for i in axes(samples, 2)
            time = start_time + round(Int64, (i - 1) * 1_000_000_000 // round(Int64, sample_rate))

            freq = 440  * 2π * 2.0^((note.key - 69) / 12)

            delta = (time - note_time) / 1_000_000_000
            
            t = freq * delta # + sin(10delta)

            signal = 0.0
            signal += sin(t)
            signal += sin(0.5t) * 0.7
            signal += sin(2t) * 0.5
            signal += sin(4t) * 0.3
            signal *= note.velocity / (127 * 15);

            attack = 10delta

            envelope = if note.end_stamp != 0
                end_time = note.end_stamp
                end_delta = (time - end_time) / 1_000_000_000.0
                release = exp((end_delta + 0.3) * -7)
                if release < eps(Float32)
                    keep_alive = false
                end

                min(attack, release)
            else
                attack
            end

            envelope = clamp(envelope, 0.0, 1.0)

            samples[:, i] .+= signal * envelope
        end
        notes[note_i] = Note(note.stamp, note.key, note.velocity, note.keep_alive || keep_alive, note.end_velocity, note.end_stamp)
    end

    nothing
end

function transform(sample_ptr::Ptr{Float32}, sample_len::UInt, notes_ptr::Ptr{Cvoid}, notes_len::UInt, sample_rate::Float32, channels::UInt, start_time::Int64)
    sample_len % channels == 0 || error("invalid buffer size given channels")
    sample = unsafe_wrap(Array, sample_ptr, (channels, sample_len ÷ channels))

    notes_ptr = convert(Ptr{Note}, notes_ptr)
    notes = unsafe_wrap(Array, notes_ptr, notes_len)

    transform(sample, notes, widen(sample_rate), start_time)

    @assert notes_ptr == pointer(notes)
end

end
