use anyhow::{anyhow, Result};
use crossbeam::channel::{unbounded, Receiver, Sender};
use log::{Level, Metadata, Record};

#[derive(Debug, Clone)]
pub struct LogRecord {
    pub level: Level,
    pub message: String,
}

pub struct CliLogger(Sender<LogRecord>);

impl CliLogger {
    pub fn install() -> Result<Receiver<LogRecord>> {
        let (log_message_sender, log_messages) = unbounded();
        let logger = Box::leak(Box::new(CliLogger(log_message_sender)));
        log::set_logger(logger).map_err(|_| anyhow!("can't set logger"))?;
        log::set_max_level(log::LevelFilter::Trace);

        Ok(log_messages)
    }
}

impl log::Log for CliLogger {
    fn enabled(&self, _metadata: &Metadata) -> bool {
        // metadata.level() <= Level::Info
        true
    }

    fn log(&self, record: &Record<'_>) {
        if self.enabled(record.metadata()) {
            let _ = self.0.send(LogRecord {
                level: record.level(),
                message: record.args().to_string(),
            });
        }
    }

    fn flush(&self) {}
}
