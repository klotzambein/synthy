use tui::layout::{Direction, Rect};

pub fn sub_div_rect(
    rect: Rect,
    direction: Direction,
    constraints: &[Constraint],
) -> impl Iterator<Item = Option<Rect>> {
    let (offset, length) = match direction {
        Direction::Horizontal => (rect.x, rect.width),
        Direction::Vertical => (rect.y, rect.height),
    };

    let mut sizes: Vec<_> = constraints.iter().map(|c| c.min()).collect();
    let mut open: Vec<_> = (0..sizes.len()).collect();

    let mut size = sizes.iter().sum::<u16>();
    while size > length {
        // Unwrap safe: when the sum is bigger than zero we must have one item.
        let lowest_priority = *open
            .iter()
            .min_by_key(|c| constraints[**c].priority())
            .unwrap();

        size -= sizes[lowest_priority];
        sizes[lowest_priority] = 0;
        let idx = open.binary_search(&lowest_priority).unwrap();
        open.remove(idx);
    }

    // Remove all non extend constraints
    let mut stars = Vec::new();
    open.retain(|c| match constraints[*c] {
        Constraint::Extend { star_value, .. } => {
            stars.push(star_value);
            true
        }
        _ => false,
    });

    let total_stars: f32 = stars.iter().sum();
    let left_over = length - size;
    stars
        .into_iter()
        // Iterate over numbers from >0.0 to 1.0
        .scan(0.0, |x, s| {
            *x += s;
            Some(*x / total_stars)
        })
        // Convert to interval from 0 to `left_over`
        .map(|s| (s * left_over as f32) as u16)
        // Convert back to individual sizes
        .scan(0, |x, l| {
            let value = l - *x;
            *x = l;
            Some(value)
        })
        // Zip with constraint indices
        .zip(open)
        .for_each(|(a, c)| sizes[c] += a);

    let mut offset = offset;
    sizes.into_iter().map(move |size| {
        if size > 0 {
            let rect = match direction {
                Direction::Horizontal => Rect::new(offset, rect.y, size, rect.height),
                Direction::Vertical => Rect::new(rect.x, offset, rect.width, size),
            };
            offset += size;
            Some(rect)
        } else {
            None
        }
    })
}

#[derive(Debug, Clone, Copy)]
pub enum Constraint {
    /// This component will always have the given length
    Length { priority: u16, length: u16 },
    /// This component will start at minimum length and fill the remaining
    /// space, according to star value
    Extend {
        priority: u16,
        min: u16,
        star_value: f32,
    },
}

impl Constraint {
    pub fn new_length(priority: u16, length: u16) -> Constraint {
        Constraint::Length { priority, length }
    }
    pub fn new_extend(priority: u16, min: u16, star_value: f32) -> Constraint {
        Constraint::Extend {
            priority,
            min,
            star_value,
        }
    }

    pub fn min(self) -> u16 {
        match self {
            Constraint::Length { length, .. } => length,
            Constraint::Extend { min, .. } => min,
        }
    }

    pub fn priority(self) -> u16 {
        match self {
            Constraint::Length { priority, .. } => priority,
            Constraint::Extend { priority, .. } => priority,
        }
    }
}

#[cfg(test)]
pub mod test {
    use super::*;

    #[test]
    fn it_sub_divs_rects() {
        let frame = Rect::new(1000, 1000, 10, 10);

        let constraints = &[
            Constraint::Length {
                priority: 10,
                length: 3,
            },
            Constraint::Extend {
                priority: 50,
                min: 5,
                star_value: 1.0,
            },
            Constraint::Length {
                priority: 100,
                length: 3,
            },
        ];

        let result: Vec<_> = sub_div_rect(frame, Direction::Vertical, constraints).collect();

        assert_eq!(
            result,
            vec![
                None,
                Some(Rect {
                    x: 1000,
                    y: 1000,
                    width: 10,
                    height: 7
                }),
                Some(Rect {
                    x: 1000,
                    y: 1007,
                    width: 10,
                    height: 3
                })
            ]
        );

        let frame_b = Rect::new(1000, 1000, 10, 20);

        let result: Vec<_> = sub_div_rect(frame_b, Direction::Vertical, constraints).collect();

        assert_eq!(
            result,
            vec![
                Some(Rect {
                    x: 1000,
                    y: 1000,
                    width: 10,
                    height: 3
                }),
                Some(Rect {
                    x: 1000,
                    y: 1003,
                    width: 10,
                    height: 14
                }),
                Some(Rect {
                    x: 1000,
                    y: 1017,
                    width: 10,
                    height: 3
                })
            ]
        );
    }
}
