#![feature(let_else)]
#![feature(drain_filter)]

use std::sync::{Arc, Mutex};

use anyhow::Error;
use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use cpal::{self, BufferSize, SampleFormat, Stream};
use log::{debug, error, info};
use midir::MidiInputConnection;
use midly::MidiMessage;

pub mod cli;
pub mod julia;
pub mod midi;
pub mod note_synth;
pub mod time;

use crate::note_synth::MidiNotes;

fn main() -> Result<(), Error> {
    let host = cpal::default_host();
    let device = host
        .default_output_device()
        .expect("failed to find a default output device");
    let configs = device.supported_output_configs()?;

    configs.for_each(|c| info!("{c:?}"));

    let config = device.default_output_config()?;
    let mut conf = config.config();
    conf.buffer_size = BufferSize::Default;
    info!("Selected: {conf:?}");

    let (midi, audio) = match config.sample_format() {
        SampleFormat::F32 => run::<f32>(&device, &conf)?,
        // SampleFormat::I16 => run::<i16>(&device, &conf)?,
        // SampleFormat::U16 => run::<u16>(&device, &conf)?,
        _ => unimplemented!(),
    };

    cli::cli()?;

    midi.close();
    audio.pause()?;

    Ok(())
}

fn run<T>(
    device: &cpal::Device,
    config: &cpal::StreamConfig,
) -> Result<(MidiInputConnection<()>, Stream), Error>
where
    T: cpal::Sample,
{
    // A channel for indicating when playback has completed.
    let sample_rate = config.sample_rate.0 as f32;

    let notes = MidiNotes::new();
    let notes = Arc::new(Mutex::new(notes));
    let notes2 = notes.clone();

    let conn_in = midi::midi_connect(move |stamp, channel, event| match event {
        MidiMessage::NoteOn { key, vel } => {
            notes.lock().unwrap().note_on(stamp, key.into(), vel.into());
        }
        MidiMessage::NoteOff { key, vel } => {
            notes
                .lock()
                .unwrap()
                .note_off(stamp.nanos, key.into(), vel.into())
        }
        m => {
            debug!("Got unknown midi message on channel {channel}: {m:?}")
        }
    })?;

    let channels = config.channels as usize;
    let jl = julia::JuliaContext::new(sample_rate, channels)?;

    // Create and run the stream.
    let err_fn = |err| error!("an error occurred on stream: {}", err);

    let stream = device.build_output_stream(
        config,
        move |data: &mut [f32], c: &cpal::OutputCallbackInfo| {
            let start_time = c
                .timestamp()
                .playback
                .duration_since(&unsafe { std::mem::zeroed() }).unwrap();

            let mut notes = notes2.lock().unwrap();

            jl.call(data, notes.notes(), start_time.as_nanos() as i64)
                .unwrap();

            notes.collect_garbage()
        },
        err_fn,
    )?;

    stream.play()?;

    Ok((conn_in, stream))
}
