use anyhow::Result;
use crossbeam::channel::{bounded, Receiver, Sender};
use jlrs::prelude::*;

use crate::note_synth::Note;

struct Request {
    sample: *mut f32,
    sample_len: usize,
    notes: *mut Note,
    notes_len: usize,
    start_time: i64,
}

unsafe impl Send for Request {}
unsafe impl Sync for Request {}

pub struct JuliaContext {
    call: Sender<Request>,
    done: Receiver<()>,
}

impl JuliaContext {
    pub fn new(sample_rate: f32, channels: usize) -> Result<JuliaContext> {
        let (call, requests) = bounded(0);
        let (submit, done) = bounded(0);
        std::thread::spawn(move || {
            JuliaContext::thread_worker(requests, submit, sample_rate, channels)
        });
        Ok(JuliaContext { call, done })
    }

    fn thread_worker(
        requests: Receiver<Request>,
        submit: Sender<()>,
        sample_rate: f32,
        channels: usize,
    ) -> Result<()> {
        // Initializing Julia is unsafe because it can race with another crate that does
        // the same.
        let mut julia = unsafe { Julia::init().unwrap() };

        julia.include("synthy.jl")?;

        let transform = julia
            .scope(|_global, frame| {
                let transform = unsafe { Value::eval_string(
                        &mut *frame,
                        "Base.@cfunction(Synthy.transform, Cvoid, (Ptr{Float32}, UInt, Ptr{Cvoid}, UInt, Float32, UInt, Int64))",
                    )}?
                    .into_jlrs_result()?
                    .data_ptr()
                    .as_ptr()
                        as *mut extern "C" fn(*mut f32, usize, *mut Note, usize, f32, usize, i64);

                Ok(unsafe { *(transform) })
            })
            .unwrap();

        dbg!(transform);

        loop {
            let request = requests.recv()?;
            transform(
                request.sample,
                request.sample_len,
                request.notes,
                request.notes_len,
                sample_rate,
                channels,
                request.start_time,
            );
            submit.send(())?;
        }
    }

    pub fn call(&self, sample: &mut [f32], notes: &mut [Note], start_time: i64) -> Result<()> {
        let req = Request {
            sample: sample.as_mut_ptr(),
            sample_len: sample.len(),
            notes: notes.as_mut_ptr(),
            notes_len: notes.len(),
            start_time,
        };
        self.call.send(req)?;
        self.done.recv()?;
        Ok(())
    }
}
