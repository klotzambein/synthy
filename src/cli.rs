use std::{
    collections::VecDeque,
    io::{self},
    time::Duration,
};

use ::log::{info, trace, warn};
use anyhow::Result;
use crossbeam::channel::Receiver;
use crossterm::{
    event::{
        self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode, MouseEvent, MouseEventKind,
    },
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use tui::{
    backend::{Backend, CrosstermBackend},
    layout::{Direction, Margin, Rect},
    style::{Color, Modifier, Style},
    symbols::DOT,
    text::Spans,
    widgets::{Block, Borders, List, ListItem, ListState, Tabs},
    Frame, Terminal,
};

pub mod layout;
pub mod log;

use self::layout::{sub_div_rect, Constraint};
use self::log::{CliLogger, LogRecord};

pub fn cli() -> Result<()> {
    // setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    let log_messages = CliLogger::install()?;
    info!("Welcome to Synthy. This is a little synthesizer written entirely in rust.");
    info!("Please select your midi instrument and your good to go.");
    warn!("Important! The person reading this is cool!!!");
    trace!("You maniac, enabling trace messages like that.");

    let mut cli = Cli::new(log_messages);

    // create app and run it
    let res = cli.run_app(&mut terminal);

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{:?}", err)
    }

    Ok(())
}

pub struct Cli {
    log_receiver: Receiver<LogRecord>,
    logs: VecDeque<LogRecord>,
    log_list: ListState,
    log_offset: usize,

    layout: Vec<(Rect, UiElement)>,
}

impl Cli {
    fn new(log_receiver: Receiver<LogRecord>) -> Cli {
        Cli {
            log_list: ListState::default(),
            logs: VecDeque::default(),
            log_offset: 0,
            log_receiver,
            layout: Vec::new(),
        }
    }

    fn run_app<B: Backend>(&mut self, terminal: &mut Terminal<B>) -> Result<()> {
        loop {
            self.recv_logs();
            terminal.draw(|f| {
                let mut layout = std::mem::take(&mut self.layout);
                layout.clear();
                layout.extend(self.layout(f.size()));
                self.layout = layout;
                self.ui(f);
            })?;

            if event::poll(Duration::from_millis(50))? {
                while event::poll(Duration::ZERO)? {
                    match event::read()? {
                        Event::Key(key) => match key.code {
                            KeyCode::Char('q') => return Ok(()),

                            KeyCode::Char('w' | 'k' | 'W' | 'K') | KeyCode::Up => {
                                self.log_offset += 1;
                                self.log_offset = self.log_offset.min(self.logs.len() - 1)
                            }

                            KeyCode::Char('s' | 'j' | 'S' | 'J') | KeyCode::Down => {
                                self.log_offset = self.log_offset.saturating_sub(1);
                            }

                            _ => (),
                        },
                        Event::Mouse(event) => self.ui_mouse_event(event),
                        _ => (),
                    }
                }
            }
        }
    }

    fn recv_logs(&mut self) {
        let old_logs_len = self.logs.len();
        self.logs.extend(self.log_receiver.try_iter());
        if self.log_offset != 0 {
            self.log_offset += self.logs.len() - old_logs_len;

            if self.log_offset != 0 && self.logs.len() > 1000 {
                let to_remove = self.logs.len() - 50;
                self.logs.drain(0..to_remove);
            }
        }
    }

    fn layout(&mut self, size: Rect) -> Vec<(Rect, UiElement)> {
        let mut laid_out_rects = Vec::new();
        let constraints = &[
            Constraint::new_length(80, 2),
            Constraint::new_extend(50, 5, 7.0),
            Constraint::new_extend(10, 5, 1.0),
            Constraint::new_length(30, 3),
        ];
        laid_out_rects.extend(
            sub_div_rect(size, Direction::Vertical, constraints)
                .zip([
                    UiElement::Tabs,
                    UiElement::Logs,
                    UiElement::Other,
                    UiElement::Bottom,
                ])
                .filter_map(|(r, u)| Some((r?, u))),
        );
        let other = laid_out_rects
            .drain_filter(|(_, elem)| elem == &UiElement::Other)
            .next();
        if let Some((r, _)) = other {
            let constraints = &[
                Constraint::new_extend(20, 15, 5.0),
                Constraint::new_extend(50, 50, 2.0),
                Constraint::new_length(30, 2),
            ];
            laid_out_rects.extend(
                sub_div_rect(r, Direction::Horizontal, constraints)
                    .zip([UiElement::Other, UiElement::Logs, UiElement::Monitor])
                    .filter_map(|(r, u)| Some((r?, u))),
            )
        }
        laid_out_rects
    }

    fn ui_mouse_event(&mut self, event: MouseEvent) {
        let MouseEvent {
            kind, column, row, ..
        } = event;
        let elem = self.layout.iter().copied().find(|(r, _)| {
            r.x <= column && r.x + r.width > column && r.y <= row && r.y + r.height > row
        });
        if let Some((r, elem)) = elem {
            match elem {
                UiElement::Tabs => {}
                UiElement::Logs => {
                    match kind {
                        MouseEventKind::ScrollUp => {
                            self.log_offset += 2;
                            self.log_offset = self.log_offset.min(self.logs.len() - 1)
                        }
                        MouseEventKind::ScrollDown => {
                            self.log_offset = self.log_offset.saturating_sub(2);
                        }
                        _ => (),
                    }
                    let _x = column - r.x + 1;
                    let _y = row - r.y + 1;
                }
                UiElement::Bottom => {}
                UiElement::Other => {}
                UiElement::Monitor => {}
            }
        }
    }

    fn ui<B: Backend>(&mut self, f: &mut Frame<B>) {
        for (r, elm) in self.layout.iter().copied() {
            match elm {
                UiElement::Tabs => {
                    let r = r.inner(&Margin {
                        vertical: 0,
                        horizontal: 1,
                    });

                    let titles = ["A", "B", "Options"];
                    let titles = titles.into_iter().map(Spans::from).collect();
                    let tabs = Tabs::new(titles)
                        .style(Style::default().add_modifier(Modifier::BOLD))
                        .highlight_style(
                            Style::default()
                                .add_modifier(Modifier::UNDERLINED)
                                .remove_modifier(Modifier::BOLD),
                        )
                        .block(Block::default().borders(Borders::BOTTOM))
                        .divider(DOT)
                        .select(0);

                    f.render_widget(tabs, r);
                }
                UiElement::Logs => {
                    let logs: Vec<_> = self
                        .logs
                        .iter()
                        .map(|l| ListItem::new(format!("{} {}", l.level, &l.message)))
                        .collect();

                    self.log_list.select(Some(logs.len() - self.log_offset - 1));
                    let list = List::new(logs)
                        .block(Block::default().borders(Borders::ALL).title("logs"))
                        .style(Style::default().fg(Color::White))
                        .highlight_style(Style::default().add_modifier(Modifier::ITALIC))
                        .highlight_symbol(">");

                    f.render_stateful_widget(list, r, &mut self.log_list);
                }
                UiElement::Other => {
                    let gauge = tui::widgets::Gauge::default()
                        .label("Hmmm")
                        .block(Block::default().borders(Borders::ALL).title("Progress"))
                        .gauge_style(
                            Style::default()
                                .fg(Color::White)
                                .bg(Color::Black)
                                .add_modifier(Modifier::ITALIC),
                        )
                        .percent(20);
                    f.render_widget(gauge, r)
                }
                UiElement::Bottom => {
                    let block = Block::default().title("Block 2").borders(Borders::ALL);
                    f.render_widget(block, r);
                }
                UiElement::Monitor => {
                    let block = Block::default().title("Monitor").borders(Borders::ALL);
                    f.render_widget(block, r);
                }
            }
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum UiElement {
    Tabs,
    Logs,
    Bottom,
    Other,
    Monitor,
}
