#[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct Time {
    pub nanos: i64,
}

impl Time {
    pub fn from_millis(millis: u64) -> Time {
        Time {
            nanos: millis as i64 * 1000,
        }
    }
}
