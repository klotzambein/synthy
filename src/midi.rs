use std::io::{stdin, stdout, Write};

use anyhow::{anyhow, bail, Result};
use log::{error, info, warn};
use midir::{Ignore, MidiInput, MidiInputConnection, MidiInputPort};
use midly::{live::LiveEvent, MidiMessage};

use crate::time::Time;

fn select_port_interactive(midi_in: &MidiInput) -> Result<MidiInputPort> {
    // Get an input port (read from console if multiple are available)
    let in_ports = midi_in.ports();
    let in_port = match in_ports.len() {
        0 => bail!("no input port found"),
        1 => {
            println!(
                "Choosing the only available input port: {}",
                midi_in.port_name(&in_ports[0]).unwrap()
            );
            in_ports[0].clone()
        }
        _ => {
            println!("\nAvailable input ports:");
            for (i, p) in in_ports.iter().enumerate() {
                println!("{}: {}", i, midi_in.port_name(p).unwrap());
            }
            print!("Please select input port: ");
            stdout().flush()?;
            let mut input = String::new();
            stdin().read_line(&mut input)?;
            in_ports
                .get(input.trim().parse::<usize>()?)
                .ok_or(anyhow!("invalid input port selected"))?
                .clone()
        }
    };

    Ok(in_port)
}

pub fn midi_connect(
    mut on_event: impl FnMut(Time, u8, MidiMessage) + Sync + Send + 'static,
) -> Result<MidiInputConnection<()>> {
    let mut midi_in = MidiInput::new("midir reading input")?;
    midi_in.ignore(Ignore::None);

    let in_port = select_port_interactive(&midi_in)?;

    let in_port_name = midi_in.port_name(&in_port)?;
    info!("opening midi connection: {in_port_name}");

    let mut last_stamp = None;

    // _conn_in needs to be a named parameter, because it needs to be kept alive until the end of the scope
    let conn_in = midi_in
        .connect(
            &in_port,
            "synthy-input",
            move |stamp, message, _| {
                let event = LiveEvent::parse(message).unwrap();

                if last_stamp
                    .map(|ls: u64| (stamp as i64 - ls as i64) < 0)
                    .unwrap_or(false)
                {
                    error!("time went backwards (midi)");
                }

                last_stamp = Some(stamp);

                let stamp = Time::from_millis(stamp);
                match event {
                    LiveEvent::Midi { channel, message } => {
                        on_event(stamp, channel.into(), message)
                    }
                    event => warn!("unknown event: {event:?}"),
                }
            },
            (),
        )
        .map_err(|_| anyhow!("midi connection failed"))?;

    info!("Connection open, reading input from '{}'", in_port_name);

    Ok(conn_in)
}
