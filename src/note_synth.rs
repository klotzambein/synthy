use crate::time::Time;
use jlrs::prelude::*;
use smallvec::SmallVec;

#[repr(C)]
#[derive(Clone, Debug, Unbox, ValidLayout, Typecheck, IntoJulia)]
#[jlrs(julia_type = "Main.Note")]
pub struct Note {
    pub stamp: i64,
    pub key: u8,
    pub velocity: u8,
    pub keep_alive: bool,
    pub end_velocity: u8,
    pub end_stamp: i64,
}

#[derive(Debug, Clone)]
pub struct MidiNotes {
    notes: SmallVec<[Note; 32]>,
}

impl MidiNotes {
    pub const fn new() -> MidiNotes {
        MidiNotes {
            notes: SmallVec::new_const(),
        }
    }

    pub fn note_on(&mut self, stamp: Time, key: u8, velocity: u8) {
        let stamp = if stamp.nanos == 0 { 1 } else { stamp.nanos };

        let idx = self
            .notes
            .binary_search_by_key(&key, |n| n.key)
            .unwrap_or_else(|e| e);

        let note = Note {
            stamp,
            key,
            velocity,
            keep_alive: false,
            end_stamp: 0,
            end_velocity: 0,
        };
        self.notes.insert(idx, note);
    }

    pub fn note_off(&mut self, stamp: i64, key: u8, _velocity: u8) {
        let stamp = if stamp == 0 { 1 } else { stamp };

        self.notes
            .iter_mut()
            .skip_while(|n| n.key != key)
            .take_while(|n| n.key == key)
            .filter(|n| n.end_stamp == 0)
            .for_each(|n| n.end_stamp = stamp);
    }

    pub fn collect_garbage(&mut self) {
        self.notes.retain(|n| {
            let keep_alive = n.keep_alive;
            n.keep_alive = false;
            n.end_stamp == 0 || keep_alive
        })
    }

    pub fn notes(&mut self) -> &mut [Note] {
        self.notes.as_mut()
    }
}

impl Default for MidiNotes {
    fn default() -> Self {
        Self::new()
    }
}
